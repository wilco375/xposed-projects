package com.wilco375.recentsfloatingapps.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.wilco375.recentsfloatingapps.BuildConfig;
import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.adapter.AppFloatingAdapter;
import com.wilco375.recentsfloatingapps.floating.FloatingManager;
import com.wilco375.recentsfloatingapps.general.IOManager;
import com.wilco375.recentsfloatingapps.general.PreferencesManager;
import com.wilco375.recentsfloatingapps.general.Utils;
import com.wilco375.recentsfloatingapps.licensing.License;
import com.wilco375.recentsfloatingapps.object.serializable.AppFloating;
import com.wilco375.recentsfloatingapps.quicksettings.Translate;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    AppWidgetManager appWidgetManager;
    AppWidgetHost appWidgetHost;

    RelativeLayout mainLayout;
    ListView listView;

    List<AppFloating> floatingApps = new ArrayList<>();

    Context context;

    static final int REQUEST_PICK_APPWIDGET = 1;
    static final int REQUEST_CREATE_APPWIDGET = 2;

    static final int REQUEST_WRITE_PERMISSION = 3;

    boolean requestOverlayPermission = false;

    private static String PRO_URL = "https://play.google.com/store/apps/details?id=com.wilco375.recentsfloatingappspro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new License().checkLicense(this);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(R.string.app_name);
        actionBar.setLogo(getResources().getDrawable(R.mipmap.ic_launcher));
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        context = this;

        // Get app widget manager and host
        appWidgetManager = AppWidgetManager.getInstance(this);
        appWidgetHost = new AppWidgetHost(this, 10);

        // Get main layout
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        assert mainLayout != null;

        // Check permissions
        checkPermissions();

        // Create Floating Action Menu
        addFloatingActionMenu();

        // Create list
        createList();

        // Check tiles
        checkTiles();
    }

    private void addFloatingActionMenu(){
        PreferencesManager preferencesManager = new PreferencesManager();
        int themeColor = Color.parseColor("#"+ preferencesManager.getString("themeColor","283593"));
        int themeColorDarker = Utils.darker(Color.parseColor("#"+preferencesManager.getString("themeColor","283593")), 0.8f);

        final FloatingActionButton calculator = new FloatingActionButton(this);
        calculator.setColorNormal(themeColor);
        calculator.setSize(FloatingActionButton.SIZE_MINI);
        calculator.setColorPressed(themeColorDarker);
        calculator.setImageDrawable(this.getResources().getDrawable(R.drawable.calc));
        calculator.setPadding(0, 0, 0, Utils.dpToPx(6, this));
        calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPro()) addItem(new AppFloating(FloatingManager.CALCULATOR));
            }
        });

        FloatingActionButton camera = new FloatingActionButton(this);
        camera.setColorNormal(themeColor);
        camera.setSize(FloatingActionButton.SIZE_MINI);
        camera.setColorPressed(themeColorDarker);
        camera.setImageDrawable(this.getResources().getDrawable(R.drawable.camera));
        camera.setPadding(0, 0, 0, Utils.dpToPx(6, this));
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPro()){
                    int hasPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && hasPermission != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, FloatingManager.CAMERA);
                    }else{
                        addItem(new AppFloating(FloatingManager.CAMERA));
                    }
                }
            }
        });

        FloatingActionButton widget = new FloatingActionButton(this);
        widget.setColorNormal(themeColor);
        widget.setSize(FloatingActionButton.SIZE_MINI);
        widget.setColorPressed(themeColorDarker);
        widget.setImageDrawable(this.getResources().getDrawable(R.drawable.widget));
        widget.setPadding(0, 0, 0, Utils.dpToPx(6, this));
        widget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPro()) selectWidget();
            }
        });

        FloatingActionButton browser = new FloatingActionButton(this);
        browser.setColorNormal(themeColor);
        browser.setSize(FloatingActionButton.SIZE_MINI);
        browser.setColorPressed(themeColorDarker);
        browser.setImageDrawable(this.getResources().getDrawable(R.drawable.browser));
        browser.setPadding(0, 0, 0, Utils.dpToPx(6, this));
        browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPro()) addItem(new AppFloating(FloatingManager.BROWSER));
            }
        });

        FloatingActionButton note = new FloatingActionButton(this);
        note.setColorNormal(themeColor);
        note.setSize(FloatingActionButton.SIZE_MINI);
        note.setColorPressed(themeColorDarker);
        note.setImageDrawable(this.getResources().getDrawable(R.drawable.note));
        note.setPadding(0, 0, 0, Utils.dpToPx(6, this));
        note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPro()) {
                    addItem(new AppFloating(FloatingManager.NOTE));
                }
            }
        });

        FloatingActionButton todo = new FloatingActionButton(this);
        todo.setColorNormal(themeColor);
        todo.setSize(FloatingActionButton.SIZE_MINI);
        todo.setColorPressed(themeColorDarker);
        todo.setImageDrawable(this.getResources().getDrawable(R.drawable.todo));
        todo.setPadding(0, 0, 0, Utils.dpToPx(6, this));
        todo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkPro()) {
                    addItem(new AppFloating(FloatingManager.TODO));
                }
            }
        });

        final FloatingActionsMenu fam = new FloatingActionsMenu(this);
        fam.addButton(calculator);
        fam.addButton(camera);
        fam.addButton(widget);
        fam.addButton(browser);
        fam.addButton(note);
        fam.addButton(todo);
        RelativeLayout.LayoutParams famParams =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        famParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        famParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        fam.setLayoutParams(famParams);
        fam.setColorNormal(Color.parseColor("#E91E63"));
        fam.setColorPressed(Utils.darker(Color.parseColor("#E91E63"), 0.8f));
        fam.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                if(checkPro()){
                    fam.collapseImmediately();
                }
            }

            @Override
            public void onMenuCollapsed() {

            }
        });

        mainLayout.addView(fam);
    }

    private boolean checkPro(){
        if(floatingApps.size() >= 3 && !BuildConfig.PRO){
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(R.string.upgrade_to_pro);
            dialog.setPositiveButton(R.string.pro_accept, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PRO_URL)));
                    dialog.cancel();
                }
            });
            dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.create().show();

            return true;
        }
        return false;
    }

    private void createList(){
        readData();

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new AppFloatingAdapter(this, floatingApps));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppFloating floatingApp = floatingApps.get(position);
                FloatingManager.launchFloatingApp(floatingApp, context);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeItem(position);
                    }
                });
                alertDialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.setMessage(R.string.confirm_delete);
                alertDialog.create().show();
                return true;
            }
        });
    }

    // Select widget
    private void selectWidget() {
        int appWidgetId = appWidgetHost.allocateAppWidgetId();
        Intent pickIntent = new Intent(AppWidgetManager.ACTION_APPWIDGET_PICK);
        pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        addEmptyData(pickIntent);
        startActivityForResult(pickIntent, REQUEST_PICK_APPWIDGET);
    }

    // Prevent bug
    private void addEmptyData(Intent pickIntent) {
        ArrayList customInfo = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_INFO, customInfo);
        ArrayList customExtras = new ArrayList();
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_EXTRAS, customExtras);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK ) {
            if (requestCode == REQUEST_PICK_APPWIDGET) {
                configureWidget(data);
            }
            else if (requestCode == REQUEST_CREATE_APPWIDGET) {
                createWidget(data);
            }
        }
        else if (resultCode == RESULT_CANCELED && data != null) {
            int appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
            if (appWidgetId != -1) {
                appWidgetHost.deleteAppWidgetId(appWidgetId);
            }
        }
    }

    // Configure widget if necessary
    private void configureWidget(Intent data) {
        Bundle extras = data.getExtras();
        int appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetProviderInfo appWidgetInfo = appWidgetManager.getAppWidgetInfo(appWidgetId);
        if (appWidgetInfo.configure != null) {
            Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
            intent.setComponent(appWidgetInfo.configure);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            startActivityForResult(intent, REQUEST_CREATE_APPWIDGET);
        } else {
            createWidget(data);
        }
    }

    // Launch a floating window with the selected widget
    private void createWidget(Intent data) {
        AppFloating floatingApp = new AppFloating();
        int widgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        floatingApp.type = FloatingManager.WIDGET;
        floatingApp.putExtraData("widgetId", widgetId);
        floatingApp.putExtraData("widgetPkg", appWidgetManager.getAppWidgetInfo(widgetId).provider.getPackageName());
        addItem(floatingApp);
    }

    private void readData(){
        if(IOManager.fileExists(IOManager.FILEPATH, IOManager.APPS_FLOATING)){
            try {
                floatingApps = (ArrayList<AppFloating>) IOManager.readObject(IOManager.FILEPATH, IOManager.APPS_FLOATING);
            }catch (ClassCastException e){
                e.printStackTrace();
            }
        }
    }

    private void saveData(){
        IOManager.writeObject(floatingApps, IOManager.FILEPATH, IOManager.APPS_FLOATING);
    }

    private void addItem(AppFloating item){
        floatingApps.add(item);
        ((ArrayAdapter)listView.getAdapter()).notifyDataSetChanged();
        saveData();
        checkTiles();
    }

    private void removeItem(int position){
        floatingApps.remove(position);
        ((ArrayAdapter)listView.getAdapter()).notifyDataSetChanged();
        saveData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        for(int i = 0; i < menu.size(); i++){
            menu.getItem(i).setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // Granted
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if(requestCode == FloatingManager.CAMERA) {
                addItem(new AppFloating(FloatingManager.CAMERA));
            }
        }
        // Not granted
        else{
            if(requestCode == REQUEST_WRITE_PERMISSION){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.write_permission_title);
                builder.setMessage(R.string.write_permission_text);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                    }
                });
                builder.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
                builder.show();
            }
        }
    }

    @TargetApi(23)
    private void checkTiles(){
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            boolean calc, camera, browser, note, todo;
            calc = camera = browser = note = todo = false;
            for (AppFloating app : floatingApps) {
                switch (app.type) {
                    case FloatingManager.BROWSER:
                        browser = true;
                        break;
                    case FloatingManager.CALCULATOR:
                        calc = true;
                        break;
                    case FloatingManager.CAMERA:
                        camera = true;
                        break;
                    case FloatingManager.NOTE:
                        note = true;
                        break;
                    case FloatingManager.TODO:
                        todo = true;
                        break;
                }
            }

            if (!browser) {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.BrowserFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP
                );
            } else {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.BrowserFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP
                );
            }
            if (!calc) {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.CalcFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP
                );
            } else {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.CalcFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP
                );
            }
            if (!camera) {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.CameraFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP
                );
            } else {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.CameraFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP
                );
            }
            if (!note) {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.NoteFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP
                );
            } else {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.NoteFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP
                );
            }
            if (!todo) {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.ToDoFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP
                );
            } else {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, com.wilco375.recentsfloatingapps.quicksettings.ToDoFloating.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP
                );
            }

            String packageName = "com.google.android.apps.translate";
            Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);

            if (intent == null) {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, Translate.class),
                        PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP
                );
            } else {
                getPackageManager().setComponentEnabledSetting(
                        new ComponentName(context, Translate.class),
                        PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                        PackageManager.DONT_KILL_APP
                );
            }
        }
    }

    @TargetApi(23)
    private void checkPermissions(){
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            int hasWritePermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            boolean hasDrawPermission = Settings.canDrawOverlays(context);

            if(hasWritePermission != PackageManager.PERMISSION_GRANTED){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.write_permission_title);
                builder.setMessage(R.string.write_permission_text);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                    }
                });
                builder.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
                builder.show();
            }

            if(!hasDrawPermission){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.draw_permission_title);
                builder.setMessage(R.string.draw_permission_text);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestOverlayPermission = true;
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                        startActivity(intent);
                    }
                });
                builder.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
                builder.show();
            }
        }
    }

    @Override
    @TargetApi(23)
    protected void onResume() {
        if(requestOverlayPermission && Build.VERSION.SDK_INT > Build.VERSION_CODES.M && !Settings.canDrawOverlays(context)){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.draw_permission_title);
            builder.setMessage(R.string.draw_permission_text);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    requestOverlayPermission = true;
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                    startActivity(intent);
                }
            });
            builder.setNeutralButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });
            builder.show();
        }
        super.onResume();
    }
}
