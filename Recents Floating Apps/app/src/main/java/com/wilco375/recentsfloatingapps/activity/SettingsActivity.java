package com.wilco375.recentsfloatingapps.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.PreferencesManager;

import java.io.DataOutputStream;
import java.io.IOException;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle("  "+getResources().getString(R.string.settings));
        actionBar.setLogo(getResources().getDrawable(R.mipmap.ic_launcher));
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        Switch showLeft = (Switch) findViewById(R.id.showLeft);
        assert showLeft != null;
        Switch closeMenu = (Switch) findViewById(R.id.closeMenu);
        assert closeMenu != null;
        Switch closeRecents = (Switch) findViewById(R.id.closeRecents);
        assert closeRecents != null;
        EditText distanceBottom = (EditText) findViewById(R.id.distanceBottom);
        assert distanceBottom != null;
        EditText distanceSide = (EditText) findViewById(R.id.distanceSide);
        assert distanceSide != null;
        EditText themeColor = (EditText) findViewById(R.id.colorTheme);
        assert themeColor != null;
        EditText menuColor = (EditText) findViewById(R.id.colorMenu);
        assert menuColor != null;

        final PreferencesManager preferencesManager = new PreferencesManager();

        showLeft.setChecked(preferencesManager.getBoolean("showLeft", false));
        closeMenu.setChecked(preferencesManager.getBoolean("closeMenu", true));
        closeRecents.setChecked(preferencesManager.getBoolean("closeRecents", true));
        distanceBottom.setText(String.valueOf(preferencesManager.getInteger("distanceBottom", 16)));
        distanceSide.setText(String.valueOf(preferencesManager.getInteger("distanceSide", 16)));
        themeColor.setText(preferencesManager.getString("themeColor", "283593"));
        menuColor.setText(preferencesManager.getString("menuColor", "E91E63"));

        showLeft.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferencesManager.putAndApply("showLeft", isChecked);
            }
        });
        closeMenu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferencesManager.putAndApply("closeMenu", isChecked);
            }
        });
        closeRecents.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preferencesManager.putAndApply("closeRecents", isChecked);
            }
        });
        distanceBottom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() != 0)
                    preferencesManager.putAndApply("distanceBottom", Integer.parseInt(s.toString()));
            }
        });
        distanceSide.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() != 0)
                    preferencesManager.putAndApply("distanceSide", Integer.parseInt(s.toString()));
            }
        });
        themeColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 3 || s.length() == 6)
                    preferencesManager.putAndApply("themeColor", s.toString());
            }
        });
        menuColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 3 || s.length() == 6)
                    preferencesManager.putAndApply("menuColor", s.toString());
            }
        });

        Button restartSystemui = (Button) findViewById(R.id.restartSystemui);
        assert restartSystemui != null;
        restartSystemui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Process suProcess = Runtime.getRuntime().exec("su");
                    DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
                    os.writeBytes("adb shell" + "\n");
                    os.flush();
                    os.writeBytes("pkill com.android.systemui" + "\n");
                    os.flush();
                }catch (IOException e){
                    Toast.makeText(SettingsActivity.this, R.string.restart_systemui_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
