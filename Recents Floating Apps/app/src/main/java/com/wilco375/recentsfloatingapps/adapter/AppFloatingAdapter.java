package com.wilco375.recentsfloatingapps.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.floating.FloatingManager;
import com.wilco375.recentsfloatingapps.object.serializable.AppFloating;

import java.util.List;

public class AppFloatingAdapter extends ArrayAdapter<AppFloating>{
    Context context;

    public AppFloatingAdapter(Context context, List<AppFloating> floatingApps) {
        super(context, 0, floatingApps);

        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AppFloating floatingApp = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_appfloating, parent, false);
        }

        ImageView icon = (ImageView) convertView.findViewById(R.id.imageView);
        //System.out.println("Type: "+floatingApp.type);
        if(floatingApp.type != FloatingManager.WIDGET) icon.setColorFilter(Color.BLACK);
        else icon.setColorFilter(Color.TRANSPARENT);
        TextView title = (TextView) convertView.findViewById(R.id.textView);

        icon.setImageBitmap(floatingApp.getBitmap(context, false));
        switch (floatingApp.type){
            case FloatingManager.CALCULATOR:
                title.setText(R.string.calculator);
                break;
            case FloatingManager.CAMERA:
                title.setText(R.string.camera);
                break;
            case FloatingManager.WIDGET:
                title.setText(R.string.widget);
                break;
            case FloatingManager.BROWSER:
                title.setText(R.string.browser);
                break;
            case FloatingManager.NOTE:
                title.setText(R.string.note);
                break;
            case FloatingManager.TODO:
                title.setText(R.string.todo);
                break;
        }

        return convertView;
    }
}
