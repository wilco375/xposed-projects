package com.wilco375.recentsfloatingapps.floating;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.PreferencesManager;
import com.wilco375.recentsfloatingapps.general.Utils;

import wei.mark.standout.StandOutWindow;
import wei.mark.standout.constants.StandOutFlags;
import wei.mark.standout.ui.Window;

public class BaseFloating extends StandOutWindow{
    RelativeLayout layoutView;
    RelativeLayout toolbar;
    ImageView resize;
    ImageView close;
    int DP48;
    int id;
    Context context;
    Window window;
    StandOutLayoutParams parameters;

    @Override
    public String getAppName() {
        return null;
    }

    @Override
    public int getAppIcon() {
        return 0;
    }

    @Override
    public StandOutLayoutParams getParams(int id, Window window) {
        this.window = window;

        StandOutLayoutParams parameters =
                new StandOutLayoutParams(
                        id,
                        Utils.percentToPx(50, this, Utils.HORIZONTAL),
                        Utils.percentToPx(50, this, Utils.VERTICAL),
                        StandOutLayoutParams.LEFT,
                        StandOutLayoutParams.TOP
                );
        parameters.x = (Utils.getScreenSize(this).x-parameters.width)/2;
        parameters.y = (Utils.getScreenSize(this).y-parameters.height)/2;

        return parameters;
    }

    @Override
    public void createAndAttachView(final int id, final FrameLayout frame) {
        context = this;
        this.id = id;
        parameters = getParams(id, window);

        DP48 = Utils.dpToPx(48, this);

        // Create layout
        layoutView = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParameteres = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutView.setBackgroundColor(Color.WHITE);
        layoutView.setLayoutParams(layoutParameteres);

        close = new ImageView(this);
        close.setId(View.generateViewId());
        close.setImageDrawable(getResources().getDrawable(R.drawable.close));
        RelativeLayout.LayoutParams closeParams = new RelativeLayout.LayoutParams(DP48, DP48);
        closeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        close.setLayoutParams(closeParams);
        close.setPadding(25, 25, 25, 25);
        // Close dialog on close click
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClose();
                close(id);
                stopSelf();
            }
        });

        toolbar = new RelativeLayout(this);
        RelativeLayout.LayoutParams toolbarParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DP48);
        toolbarParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        toolbar.setLayoutParams(toolbarParams);
        toolbar.setBackground(getResources().getDrawable(R.drawable.floating_toolbar_bg));
        ((GradientDrawable)(((LayerDrawable)toolbar.getBackground()).getDrawable(1))).setColor(Color.parseColor("#"+ (new PreferencesManager()).getString("themeColor","283593")));
        toolbar.addView(close);
        toolbar.setId(View.generateViewId());

        // Move window
        toolbar.setOnTouchListener(new View.OnTouchListener() {

            StandOutLayoutParams updatedParameters = parameters;
            double x;
            double y;
            double pressedX;
            double pressedY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        x = updatedParameters.x;
                        y = updatedParameters.y;

                        pressedX = event.getRawX();
                        pressedY = event.getRawY();

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        updatedParameters.x = (int) (x + (event.getRawX() - pressedX));
                        updatedParameters.y = (int) (y + (event.getRawY() - pressedY));

                        updateViewLayout(id, updatedParameters);
                        return true;
                    default:
                        break;
                }
                return false;
            }
        });

        resize = new ImageView(this);
        resize.setImageDrawable(getResources().getDrawable(R.drawable.resize));
        final RelativeLayout.LayoutParams resizeParams = new RelativeLayout.LayoutParams(Utils.dpToPx(32, this), Utils.dpToPx(32, this));
        resizeParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        resizeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        resize.setId(View.generateViewId());
        resize.setLayoutParams(resizeParams);

        // Resize window
        resize.setOnTouchListener(new View.OnTouchListener() {
            StandOutLayoutParams updatedParameters = parameters;
            double pressedX;
            double pressedY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pressedX = event.getRawX();
                        pressedY = event.getRawY();

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        updatedParameters.width = (int) (updatedParameters.width + (event.getRawX() - pressedX));
                        updatedParameters.height = (int) (updatedParameters.height + (event.getRawY() - pressedY));
                        pressedX = event.getRawX();
                        pressedY = event.getRawY();


                        updateViewLayout(id, updatedParameters);
                        updateUI();
                        resize.bringToFront();
                        return true;
                    case MotionEvent.ACTION_UP:
                        afterResize();
                        resize.bringToFront();
                    default:
                        break;
                }
                return false;
            }
        });

        layoutView.addView(toolbar);
        layoutView.addView(resize);
        frame.addView(layoutView);

        createUI();
        resize.bringToFront();
    }

    public void updateUI(){}

    public void createUI(){}

    public void afterResize(){}

    public void onClose(){}

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int getFlags(int id) {
        return super.getFlags(id) | StandOutFlags.FLAG_WINDOW_FOCUS_INDICATOR_DISABLE;
    }

    @Override
    public Notification getPersistentNotification(int id) {
        return null;
    }
}
