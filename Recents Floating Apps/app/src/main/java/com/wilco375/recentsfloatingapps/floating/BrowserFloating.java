package com.wilco375.recentsfloatingapps.floating;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wilco375.recentsfloatingapps.R;

public class BrowserFloating extends BaseFloating{
    Context context;

    @Override
    public void createUI() {
        context = this;

        final WebView webView = new WebView(this);
        WebSettings webSettings = webView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSavePassword(false);
        webSettings.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= 11) {
            webSettings.setDisplayZoomControls(false);
        }
        loadUrl(webView, getResources().getString(R.string.default_url));

        final EditText urlInput = new EditText(this);
        RelativeLayout.LayoutParams urlInputParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DP48);
        urlInputParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        urlInputParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        urlInput.setLayoutParams(urlInputParams);
        urlInput.setId(View.generateViewId());
        urlInput.setTextColor(Color.BLACK);
        urlInput.setGravity(Gravity.CENTER_VERTICAL);
        urlInput.setSingleLine(true);
        urlInput.setSelectAllOnFocus(true);
        urlInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus && !webView.getUrl().equals(urlInput.getText().toString())){
                    String url = urlInput.getText().toString();
                    if(!url.contains("://")) url = "http://"+url;
                    loadUrl(webView, url);
                }
            }
        });
        urlInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(!webView.getUrl().equals(urlInput.getText().toString())){
                    String url = urlInput.getText().toString();
                    if(!url.contains("://")) url = "http://"+url;
                    loadUrl(webView, url);
                    urlInput.clearFocus();
                    return true;
                }
                return false;
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                loadUrl(view, url);
                urlInput.setText(url);
                return true;
            }
        });
        RelativeLayout.LayoutParams webViewParams =
                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        webViewParams.addRule(RelativeLayout.BELOW, urlInput.getId());
        webView.setLayoutParams(webViewParams);

        layoutView.addView(webView);
        layoutView.addView(urlInput);
    }

    private void loadUrl(final WebView webView, final String url){
        new Thread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(url);
            }
        }).run();
    }
}
