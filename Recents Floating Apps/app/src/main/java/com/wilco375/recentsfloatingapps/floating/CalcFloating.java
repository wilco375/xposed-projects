package com.wilco375.recentsfloatingapps.floating;

import android.graphics.Color;
import android.os.Build;
import android.text.method.ScrollingMovementMethod;
import android.text.method.Touch;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.PreferencesManager;
import com.wilco375.recentsfloatingapps.general.Utils;

import java.text.DecimalFormat;

public class CalcFloating extends BaseFloating{
    DecimalFormat fiveDecimals = new DecimalFormat("0.#####");

    TextView one;
    TextView two;
    TextView three;
    TextView four;
    TextView five;
    TextView six;
    TextView seven;
    TextView eight;
    TextView nine;
    TextView zero;
    TextView minus;
    TextView plus;
    TextView multiply;
    TextView divide;
    TextView comma;
    TextView result;

    @Override
    public void createUI(){
        PreferencesManager preferencesManager = new PreferencesManager();
        int themeColor = Color.parseColor("#"+preferencesManager.getString("themeColor","283593"));
        int themeColorDarker = Utils.darker(Color.parseColor("#"+preferencesManager.getString("themeColor","283593")), 0.8f);

        final TextView answer = new TextView(this);
        answer.setText("");
        answer.setTextColor(Color.BLACK);
        RelativeLayout.LayoutParams answerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(64, this));
        answerParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        answerParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        answerParams.leftMargin = Utils.dpToPx(8, this);
        answerParams.rightMargin = Utils.dpToPx(56, this);
        answer.setTextSize(32);
        answer.setLayoutParams(answerParams);
        answer.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
        answer.setId(View.generateViewId());
        answer.setMovementMethod(new ScrollingMovementMethod());
        answer.setHorizontallyScrolling(true);
        answer.setSingleLine(true);
        layoutView.addView(answer);

        ImageView backspace = new ImageView(this);
        backspace.setImageDrawable(getResources().getDrawable(R.drawable.back));
        backspace.setColorFilter(themeColorDarker);
        RelativeLayout.LayoutParams backspaceParams = new RelativeLayout.LayoutParams(DP48, DP48);
        backspaceParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        backspaceParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        backspaceParams.topMargin = Utils.dpToPx(8, this);
        backspace.setLayoutParams(backspaceParams);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer.length() > 0)
                    answer.setText(answer.getText().toString().substring(0, answer.length() - 1));
            }
        });
        backspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                answer.setText("");
                return true;
            }
        });
        layoutView.addView(backspace);

        int keyWidth = parameters.width/4 + 1;
        int keyHeight = (parameters.height-DP48-Utils.dpToPx(64, this))/4 + 1;

        seven = new TextView(this);
        RelativeLayout.LayoutParams sevenParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        sevenParams.addRule(RelativeLayout.BELOW, answer.getId());
        sevenParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        seven.setLayoutParams(sevenParams);
        if(keyHeight > DP48)
            seven.setTextSize(32);
        else seven.setTextSize(24);
        seven.setTextColor(Color.WHITE);
        seven.setText("7");
        seven.setGravity(Gravity.CENTER);
        seven.setId(View.generateViewId());
        seven.setBackgroundColor(themeColor);
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "7");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(seven);

        eight = new TextView(this);
        RelativeLayout.LayoutParams eightParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        eightParams.addRule(RelativeLayout.BELOW, answer.getId());
        eightParams.addRule(RelativeLayout.RIGHT_OF, seven.getId());
        eight.setLayoutParams(eightParams);
        if(keyHeight > DP48)
            eight.setTextSize(32);
        else eight.setTextSize(24);
        eight.setTextColor(Color.WHITE);
        eight.setGravity(Gravity.CENTER);
        eight.setText("8");
        eight.setId(View.generateViewId());
        eight.setBackgroundColor(themeColor);
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "8");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(eight);

        nine = new TextView(this);
        RelativeLayout.LayoutParams nineParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        nineParams.addRule(RelativeLayout.BELOW, answer.getId());
        nineParams.addRule(RelativeLayout.RIGHT_OF, eight.getId());
        nine.setLayoutParams(nineParams);
        if(keyHeight > DP48)
            nine.setTextSize(32);
        else nine.setTextSize(24);
        nine.setTextColor(Color.WHITE);
        nine.setGravity(Gravity.CENTER);
        nine.setBackgroundColor(themeColor);
        nine.setText("9");
        nine.setId(View.generateViewId());
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "9");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(nine);

        divide = new TextView(this);
        RelativeLayout.LayoutParams divideParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        divideParams.addRule(RelativeLayout.BELOW, answer.getId());
        divideParams.addRule(RelativeLayout.RIGHT_OF, nine.getId());
        divide.setLayoutParams(divideParams);
        if(keyHeight > DP48)
            divide.setTextSize(32);
        else divide.setTextSize(24);
        divide.setTextColor(Color.WHITE);
        divide.setGravity(Gravity.CENTER);
        divide.setBackgroundColor(themeColorDarker);
        divide.setText("/");
        divide.setId(View.generateViewId());
        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "/");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(divide);

        four = new TextView(this);
        RelativeLayout.LayoutParams fourParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        fourParams.addRule(RelativeLayout.BELOW, seven.getId());
        fourParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        four.setLayoutParams(fourParams);
        if(keyHeight > DP48)
            four.setTextSize(32);
        else four.setTextSize(24);
        four.setTextColor(Color.WHITE);
        four.setText("4");
        four.setGravity(Gravity.CENTER);
        four.setId(View.generateViewId());
        four.setBackgroundColor(themeColor);
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "4");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(four);

        five = new TextView(this);
        RelativeLayout.LayoutParams fiveParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        fiveParams.addRule(RelativeLayout.BELOW, eight.getId());
        fiveParams.addRule(RelativeLayout.RIGHT_OF, four.getId());
        five.setLayoutParams(fiveParams);
        if(keyHeight > DP48)
            five.setTextSize(32);
        else five.setTextSize(24);
        five.setTextColor(Color.WHITE);
        five.setGravity(Gravity.CENTER);
        five.setText("5");
        five.setId(View.generateViewId());
        five.setBackgroundColor(themeColor);
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "5");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(five);

        six = new TextView(this);
        RelativeLayout.LayoutParams sixParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        sixParams.addRule(RelativeLayout.BELOW, nine.getId());
        sixParams.addRule(RelativeLayout.RIGHT_OF, five.getId());
        six.setLayoutParams(sixParams);
        if(keyHeight > DP48)
            six.setTextSize(32);
        else six.setTextSize(24);
        six.setTextColor(Color.WHITE);
        six.setGravity(Gravity.CENTER);
        six.setBackgroundColor(themeColor);
        six.setText("6");
        six.setId(View.generateViewId());
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "6");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(six);

        multiply = new TextView(this);
        RelativeLayout.LayoutParams multiplyParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        multiplyParams.addRule(RelativeLayout.BELOW, divide.getId());
        multiplyParams.addRule(RelativeLayout.RIGHT_OF, six.getId());
        multiply.setLayoutParams(multiplyParams);
        if(keyHeight > DP48)
            multiply.setTextSize(32);
        else multiply.setTextSize(24);
        multiply.setTextColor(Color.WHITE);
        multiply.setGravity(Gravity.CENTER);
        multiply.setBackgroundColor(themeColorDarker);
        multiply.setText("*");
        multiply.setId(View.generateViewId());
        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "*");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(multiply);

        one = new TextView(this);
        RelativeLayout.LayoutParams oneParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        oneParams.addRule(RelativeLayout.BELOW, four.getId());
        oneParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        one.setLayoutParams(oneParams);
        if(keyHeight > DP48)
            one.setTextSize(32);
        else one.setTextSize(24);
        one.setTextColor(Color.WHITE);
        one.setText("1");
        one.setGravity(Gravity.CENTER);
        one.setId(View.generateViewId());
        one.setBackgroundColor(themeColor);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "1");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(one);

        two = new TextView(this);
        RelativeLayout.LayoutParams twoParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        twoParams.addRule(RelativeLayout.BELOW, five.getId());
        twoParams.addRule(RelativeLayout.RIGHT_OF, one.getId());
        two.setLayoutParams(twoParams);
        if(keyHeight > DP48)
            two.setTextSize(32);
        else two.setTextSize(24);
        two.setTextColor(Color.WHITE);
        two.setGravity(Gravity.CENTER);
        two.setText("2");
        two.setId(View.generateViewId());
        two.setBackgroundColor(themeColor);
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "2");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(two);

        three = new TextView(this);
        RelativeLayout.LayoutParams threeParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        threeParams.addRule(RelativeLayout.BELOW, six.getId());
        threeParams.addRule(RelativeLayout.RIGHT_OF, two.getId());
        three.setLayoutParams(threeParams);
        if(keyHeight > DP48)
            three.setTextSize(32);
        else three.setTextSize(24);
        three.setTextColor(Color.WHITE);
        three.setGravity(Gravity.CENTER);
        three.setBackgroundColor(themeColor);
        three.setText("3");
        three.setId(View.generateViewId());
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "3");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(three);

        plus = new TextView(this);
        RelativeLayout.LayoutParams plusParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        plusParams.addRule(RelativeLayout.BELOW, multiply.getId());
        plusParams.addRule(RelativeLayout.RIGHT_OF, three.getId());
        plus.setLayoutParams(plusParams);
        if(keyHeight > DP48)
            plus.setTextSize(32);
        else plus.setTextSize(24);
        plus.setTextColor(Color.WHITE);
        plus.setGravity(Gravity.CENTER);
        plus.setBackgroundColor(themeColorDarker);
        plus.setText("+");
        plus.setId(View.generateViewId());
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "+");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(plus);

        zero = new TextView(this);
        RelativeLayout.LayoutParams zeroParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        zeroParams.addRule(RelativeLayout.BELOW, one.getId());
        zeroParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        zero.setLayoutParams(zeroParams);
        if(keyHeight > DP48)
            zero.setTextSize(32);
        else zero.setTextSize(24);
        zero.setTextColor(Color.WHITE);
        zero.setText("0");
        zero.setGravity(Gravity.CENTER);
        zero.setId(View.generateViewId());
        zero.setBackgroundColor(themeColor);
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "0");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(zero);

        comma = new TextView(this);
        RelativeLayout.LayoutParams commaParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        commaParams.addRule(RelativeLayout.BELOW, two.getId());
        commaParams.addRule(RelativeLayout.RIGHT_OF, zero.getId());
        comma.setLayoutParams(commaParams);
        if(keyHeight > DP48)
            comma.setTextSize(32);
        else comma.setTextSize(24);
        comma.setTextColor(Color.WHITE);
        comma.setGravity(Gravity.CENTER);
        comma.setText(".");
        comma.setId(View.generateViewId());
        comma.setBackgroundColor(themeColor);
        comma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + ".");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(comma);

        result = new TextView(this);
        RelativeLayout.LayoutParams resultParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        resultParams.addRule(RelativeLayout.BELOW, three.getId());
        resultParams.addRule(RelativeLayout.RIGHT_OF, comma.getId());
        result.setLayoutParams(resultParams);
        if(keyHeight > DP48)
            result.setTextSize(32);
        else result.setTextSize(24);
        result.setTextColor(Color.WHITE);
        result.setGravity(Gravity.CENTER);
        result.setBackgroundColor(themeColor);
        result.setText("=");
        result.setId(View.generateViewId());
        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double calcResult = Utils.calc(answer.getText().toString());
                    answer.setText(fiveDecimals.format(calcResult).replace(",", "."));
                }catch (RuntimeException e){
                    answer.setText("ERROR");
                }
            }
        });
        layoutView.addView(result);

        minus = new TextView(this);
        RelativeLayout.LayoutParams minusParams = new RelativeLayout.LayoutParams(keyWidth, keyHeight);
        minusParams.addRule(RelativeLayout.BELOW, plus.getId());
        minusParams.addRule(RelativeLayout.RIGHT_OF, result.getId());
        minus.setLayoutParams(minusParams);
        if(keyHeight > DP48)
            minus.setTextSize(32);
        else minus.setTextSize(24);
        minus.setTextColor(Color.WHITE);
        minus.setGravity(Gravity.CENTER);
        minus.setBackgroundColor(themeColorDarker);
        minus.setText("-");
        minus.setId(View.generateViewId());
        if(Build.VERSION.SDK_INT >= 21) minus.setZ(-1);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText(answer.getText().toString() + "-");
                Touch.scrollTo(answer, answer.getLayout(), Integer.MAX_VALUE, 0);
            }
        });
        layoutView.addView(minus);
    }

    @Override
    public void updateUI() {
        int keyWidth = parameters.width/4 + 1;
        int keyHeight = (parameters.height-DP48-Utils.dpToPx(64, this))/4 + 1;

        one.getLayoutParams().width = keyWidth;
        two.getLayoutParams().width = keyWidth;
        three.getLayoutParams().width = keyWidth;
        four.getLayoutParams().width = keyWidth;
        five.getLayoutParams().width = keyWidth;
        six.getLayoutParams().width = keyWidth;
        seven.getLayoutParams().width = keyWidth;
        eight.getLayoutParams().width = keyWidth;
        nine.getLayoutParams().width = keyWidth;
        zero.getLayoutParams().width = keyWidth;
        minus.getLayoutParams().width = keyWidth;
        plus.getLayoutParams().width = keyWidth;
        multiply.getLayoutParams().width = keyWidth;
        divide.getLayoutParams().width = keyWidth;
        comma.getLayoutParams().width = keyWidth;
        result.getLayoutParams().width = keyWidth;

        one.getLayoutParams().height = keyHeight;
        two.getLayoutParams().height = keyHeight;
        three.getLayoutParams().height = keyHeight;
        four.getLayoutParams().height = keyHeight;
        five.getLayoutParams().height = keyHeight;
        six.getLayoutParams().height = keyHeight;
        seven.getLayoutParams().height = keyHeight;
        eight.getLayoutParams().height = keyHeight;
        nine.getLayoutParams().height = keyHeight;
        zero.getLayoutParams().height = keyHeight;
        minus.getLayoutParams().height = keyHeight;
        plus.getLayoutParams().height = keyHeight;
        multiply.getLayoutParams().height = keyHeight;
        divide.getLayoutParams().height = keyHeight;
        comma.getLayoutParams().height = keyHeight;
        result.getLayoutParams().height = keyHeight;
    }
}
