package com.wilco375.recentsfloatingapps.floating;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.hardware.Camera;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.CameraPreview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraFloating extends BaseFloating{
    boolean cameraSupport = false;
    Camera camera = null;
    int cameraPreviewId = View.generateViewId();
    Context context;
    int rotation;

    @Override
    public void onCreate() {
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            cameraSupport = true;
        }else{
            Toast.makeText(this, R.string.no_camera_error, Toast.LENGTH_LONG).show();
        }

        context = this;

        super.onCreate();
    }

    @Override
    public void createUI() {
        super.createUI();

        if(cameraSupport){
            try{
                camera = Camera.open();
                Camera.Size pictureSize = camera.getParameters().getSupportedPictureSizes().get(0);
                for(Camera.Size size : camera.getParameters().getSupportedPictureSizes()){
                    if(size.width > pictureSize.width && size.height > pictureSize.height) pictureSize = size;
                }
                camera.getParameters().setPictureSize(pictureSize.width, pictureSize.height);
                setCameraDisplayOrientation();
                createCameraPreview();
            }catch (Exception e){
                Toast.makeText(this, R.string.camera_unavailable_error, Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateUI() {
        layoutView.removeView(layoutView.findViewById(cameraPreviewId));
    }

    @Override
    public void afterResize() {
        createCameraPreview();
    }

    private void createCameraPreview(){
        CameraPreview cameraPreview = new CameraPreview(this, camera);
        RelativeLayout.LayoutParams cameraPreviewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        cameraPreviewParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        cameraPreview.setLayoutParams(cameraPreviewParams);
        cameraPreview.setId(cameraPreviewId);
        cameraPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
                    camera.autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean success, Camera camera) {
                            camera.takePicture(null, null, savePicture);
                        }
                    });
                }else camera.takePicture(null, null, savePicture);
            }
        });
        layoutView.addView(cameraPreview);
    }

    private void setCameraDisplayOrientation(){
        Camera.Parameters parameters = camera.getParameters();
        Display display = ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();

        if(display.getRotation() == Surface.ROTATION_0){
            camera.setDisplayOrientation(90);
            rotation = 90;
        }else if(display.getRotation() == Surface.ROTATION_270){
            camera.setDisplayOrientation(180);
            rotation = 180;
        }else rotation = 0;
        camera.setParameters(parameters);
    }

    private Camera.PictureCallback savePicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile();
            if (pictureFile == null) return;

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();

                if(rotation != 0) rotateImage(rotation, pictureFile);
                updateMediaScanner(pictureFile);

                camera.startPreview();
                Toast.makeText(context, R.string.picture_saved, Toast.LENGTH_SHORT).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private void updateMediaScanner(File file){
        Uri uri = Uri.fromFile(file);
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +"IMG_"+ timeStamp + ".jpg");
    }

    private void rotateImage(int degrees, File image){
        Bitmap bmp = BitmapFactory.decodeFile(image.getAbsolutePath());

        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(image);
            bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void onClose() {
        if(camera != null) camera.release();
        super.onClose();

    }

    @Override
    public void onDestroy() {
        if(camera != null) camera.release();
        super.onDestroy();
    }
}
