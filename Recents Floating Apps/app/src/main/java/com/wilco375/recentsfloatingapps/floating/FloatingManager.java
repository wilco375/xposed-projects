package com.wilco375.recentsfloatingapps.floating;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;

import com.wilco375.recentsfloatingapps.BuildConfig;
import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.object.serializable.AppFloating;

import wei.mark.standout.StandOutWindow;

public class FloatingManager {
    public static final int CALCULATOR = 0;
    public static final int WIDGET = 1;
    public static final int CAMERA = 2;
    public static final int BROWSER = 3;
    public static final int NOTE = 4;
    public static final int TODO = 5;

    private static final String PACKAGE_NAME = "com.wilco375.recentsfloatingapps";

    public static void launchFloatingApp(AppFloating floatingApp, Context context){
        Intent i = new Intent();
        switch (floatingApp.type){
            case CALCULATOR:
                i.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME+".floating.CalcFloating");
                break;
            case WIDGET:
                i.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME+".floating.WidgetFloating");
                i.putExtra("widgetId", (int) floatingApp.getExtraData("widgetId"));
                break;
            case CAMERA:
                i.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME+".floating.CameraFloating");
                break;
            case BROWSER:
                i.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME+".floating.BrowserFloating");
                break;
            case NOTE:
                i.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME+".floating.NoteFloating");
                break;
            case TODO:
                i.setClassName(BuildConfig.APPLICATION_ID, PACKAGE_NAME+".floating.ToDoFloating");
                break;
            default:
                return;
        }
        context.startService(i.putExtra("id", StandOutWindow.DEFAULT_ID).setAction("SHOW"));
    }

    public static Bitmap getBitmap(AppFloating app, Context context, boolean scale){
        switch (app.type){
            case FloatingManager.CALCULATOR:
                return createBitmap(context.getResources().getDrawable(R.drawable.calc), context, scale);
            case FloatingManager.CAMERA:
                return createBitmap(context.getResources().getDrawable(R.drawable.camera), context, scale);
            case FloatingManager.WIDGET:
                try {
                    return createBitmap(context.getPackageManager().getApplicationIcon((String) app.getExtraData("widgetPkg")), context, scale);
                }catch (PackageManager.NameNotFoundException e){
                    return createBitmap(context.getResources().getDrawable(R.drawable.widget), context, scale);
                }
            case FloatingManager.BROWSER:
                return createBitmap(context.getResources().getDrawable(R.drawable.browser), context, scale);
            case FloatingManager.NOTE:
                return createBitmap(context.getResources().getDrawable(R.drawable.note), context, scale);
            case FloatingManager.TODO:
                return createBitmap(context.getResources().getDrawable(R.drawable.todo), context, scale);
            default:
                return null;
        }
    }

    private static Bitmap createBitmap(Drawable drawable, Context context, boolean scale){
        if(Build.VERSION.SDK_INT >= 21 && drawable instanceof VectorDrawable){
            Bitmap bitmap;
            if(scale) {
                bitmap = Bitmap.createBitmap(
                        (int) context.getResources().getDimension(R.dimen.fab_icon_size),
                        (int) context.getResources().getDimension(R.dimen.fab_icon_size),
                        Bitmap.Config.ARGB_8888);
            }else{
                bitmap = Bitmap.createBitmap(
                        drawable.getIntrinsicWidth(),
                        drawable.getIntrinsicHeight(),
                        Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }else {
            //System.out.println("Scaling to bitmap with size of "+(int) context.getResources().getDimension(R.dimen.fab_icon_size));
            if(scale) {
                return Bitmap.createScaledBitmap(
                        ((BitmapDrawable) drawable).getBitmap(),
                        (int) context.getResources().getDimension(R.dimen.fab_icon_size),
                        (int) context.getResources().getDimension(R.dimen.fab_icon_size),
                        false
                );
            }else{
                return ((BitmapDrawable) drawable).getBitmap();
            }
        }
    }
}
