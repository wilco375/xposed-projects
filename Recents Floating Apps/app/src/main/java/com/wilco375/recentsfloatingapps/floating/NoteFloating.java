package com.wilco375.recentsfloatingapps.floating;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.IOManager;

import java.util.List;

public class NoteFloating extends BaseFloating{
    EditText editText;

    @Override
    public void createUI() {
        ImageView save = new ImageView(this);
        save.setImageResource(R.drawable.save);
        RelativeLayout.LayoutParams saveParams = new RelativeLayout.LayoutParams(DP48, DP48);
        saveParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        saveParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        save.setLayoutParams(saveParams);
        save.setPadding(25, 25, 25, 25);
        save.setId(View.generateViewId());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle(R.string.save_as);
                final EditText name = new EditText(context);
                dialog.setView(name);
                dialog.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        IOManager.writeObject(editText.getText().toString(), IOManager.FILEPATH, name.getText().toString()+".txt");
                        Toast.makeText(context, R.string.file_saved, Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = dialog.create();
                alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alertDialog.show();
            }
        });

        ImageView open = new ImageView(this);
        open.setImageResource(R.drawable.open);
        RelativeLayout.LayoutParams openParams = new RelativeLayout.LayoutParams(DP48, DP48);
        openParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        openParams.addRule(RelativeLayout.RIGHT_OF, save.getId());
        open.setLayoutParams(openParams);
        open.setPadding(25, 25, 25, 25);
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle(R.string.open_file);
                final ListView listView = new ListView(context);
                final List<String> files = IOManager.getTextFiles(IOManager.FILEPATH);
                listView.setAdapter(new ArrayAdapter<>(context,android.R.layout.simple_list_item_1 , files));
                dialog.setView(listView);
                dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                final AlertDialog alertDialog = dialog.create();
                alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alertDialog.show();
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String fileName = files.get(position);
                        editText.setText(IOManager.readString(IOManager.FILEPATH, fileName+".txt"));
                        Toast.makeText(context, R.string.file_loaded, Toast.LENGTH_SHORT).show();
                        alertDialog.cancel();
                    }
                });
            }
        });

        editText = new EditText(this);
        RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        editTextParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        editText.setLayoutParams(editTextParams);
        editText.setBackgroundColor(Color.TRANSPARENT);
        editText.setGravity(Gravity.TOP);
        editText.setTextColor(Color.BLACK);

        layoutView.addView(save);
        layoutView.addView(open);
        layoutView.addView(editText);
    }
}
