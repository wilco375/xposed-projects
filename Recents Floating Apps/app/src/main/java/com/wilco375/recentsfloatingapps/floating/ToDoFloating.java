package com.wilco375.recentsfloatingapps.floating;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.IOManager;
import com.wilco375.recentsfloatingapps.layout.ToDoListView;

import java.util.ArrayList;
import java.util.List;

public class ToDoFloating extends BaseFloating{
    ToDoListView toDoListView;

    @Override
    public void createUI() {
        super.createUI();

        String texts = IOManager.readString(IOManager.FILEPATH, "todolist.todo");
        List<String> textsList = new ArrayList<>();
        for(String text : texts.split("\\n")){
            textsList.add(text);
        }
        toDoListView = new ToDoListView(context, textsList);

        ScrollView scrollView = new ScrollView(context);
        RelativeLayout.LayoutParams scrollViewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        scrollViewParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        scrollView.setLayoutParams(scrollViewParams);
        scrollView.addView(toDoListView);
        layoutView.addView(scrollView);

        ImageView save = new ImageView(this);
        save.setImageResource(R.drawable.save);
        RelativeLayout.LayoutParams saveParams = new RelativeLayout.LayoutParams(DP48, DP48);
        saveParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        saveParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        save.setLayoutParams(saveParams);
        save.setPadding(25, 25, 25, 25);
        save.setId(View.generateViewId());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IOManager.writeObject(toDoListView.getTextsAsString(), IOManager.FILEPATH, "todolist.todo");
                Toast.makeText(context, R.string.file_saved, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onClose() {
        IOManager.writeObject(toDoListView.getTextsAsString(), IOManager.FILEPATH, "todolist.todo");
    }
}
