package com.wilco375.recentsfloatingapps.floating;

import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class WidgetFloating extends BaseFloating{

    int widgetId = -1;
    boolean uiCreated = false;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null && intent.getExtras() != null){
            widgetId = intent.getExtras().getInt("widgetId");
        }

        if(uiCreated) {
            createUI();
            resize.bringToFront();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void createUI() {
        uiCreated = true;
        if(widgetId > 0) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
            AppWidgetHost appWidgetHost = new AppWidgetHost(this, 10);

            AppWidgetProviderInfo widgetInfo = appWidgetManager.getAppWidgetInfo(widgetId);
            AppWidgetHostView hostView = appWidgetHost.createView(getApplicationContext(), widgetId, widgetInfo);
            hostView.setAppWidget(widgetId, widgetInfo);
            RelativeLayout.LayoutParams hostViewParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            hostViewParams.addRule(RelativeLayout.BELOW, toolbar.getId());
            hostViewParams.setMargins(0, 0, 0, 0);
            hostView.setLayoutParams(hostViewParams);
            hostView.setPadding(0, 0, 0, 0);
            appWidgetHost.startListening();

            layoutView.addView(hostView);
        }
    }
}
