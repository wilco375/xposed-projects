package com.wilco375.recentsfloatingapps.general;

import android.os.Environment;

import com.wilco375.recentsfloatingapps.BuildConfig;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class IOManager {

    // Filepath
    public static String FILEPATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/data/com.wilco375.recentsfloatingapps/";

    // Config files
    public static String PREFS = "prefs.ser";

    // Floating apps file
    public static String APPS_FLOATING = "appsFloating.ser";

    /**
     * Write Object to specified file
     * @param obj Object to write
     * @param filePath Path to file to write to
     * @param name Name of file to write to
     */
    public static void writeObject(Object obj,String filePath, String name) {
        try {
            //System.out.println("Writing to "+filePath+name);
            File file = new File(filePath+name);
            file.getParentFile().mkdirs();
            if(!(new File(filePath+".nomedia")).exists()) (new File(filePath+".nomedia")).createNewFile();
            if(file.exists()) file.delete();
            if(file.createNewFile()) {
                file.setReadable(true, false);
                if(!(obj instanceof  String)) {
                    FileOutputStream fos = new FileOutputStream(file);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(obj);
                    oos.close();
                }else{
                    FileWriter fileWriter = new FileWriter(file);
                    fileWriter.write((String) obj);
                    fileWriter.close();
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read Object from specified file
     * @param filePath Path to file to read from
     * @param name Name of file to read from
     * @return Read object or new Object() if non existing file
     */
    public static Object readObject(String filePath, String name) {
        try {
            File file = new File(filePath+name);
            if (!file.exists()){
                return new Object();
            }
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            return ois.readObject();
        }catch (IOException e) {
            e.printStackTrace();
            return new Object();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
            return new Object();
        }
    }

    public static String readString(String filePath, String name){
        try {
            File file = new File(filePath+name);
            if (!file.exists()){
                return "";
            }
            InputStream inputStream = new FileInputStream(file);

            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString;
            StringBuilder stringBuilder = new StringBuilder();

            while ((receiveString = bufferedReader.readLine()) != null) {
                if(stringBuilder.length() > 0) stringBuilder.append("\n");
                stringBuilder.append(receiveString);
            }

            inputStream.close();
            return stringBuilder.toString();
        }catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Check if specified file exists
     * @param filePath Path to file to check
     * @param name Name of file to check
     * @return File exists
     */
    public static boolean fileExists(String filePath, String name) {
        File file = new File(filePath+name);
        if (!file.exists()){
            return false;
        }
        return true;
    }

    /**
     * Delete specified file
     * @param filePath Path to file to delete
     * @param name Name of file to delete
     */
    public static void deleteFile(String filePath, String name) {
        File file = new File(filePath+name);
        if (file.exists()){
            file.delete();
        }
    }

    public static List<String> getTextFiles(String filePath){
        List<String> files = new ArrayList<>();
        File path = new File(filePath);
        for(File file : path.listFiles()){
            if(file.getName().endsWith(".txt"))
                files.add(file.getName().replace(".txt", ""));
        }
        return files;
    }
}
