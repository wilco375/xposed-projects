package com.wilco375.recentsfloatingapps.general;

import com.wilco375.recentsfloatingapps.object.serializable.DefaultHashMap;

/**
 * Alternative to SharedPreferences that does not seem to be working correctly in Android 6.0 with Xposed
 */
public class PreferencesManager {

    DefaultHashMap<String, Object> prefs;

    public PreferencesManager() {
        if (IOManager.fileExists(IOManager.FILEPATH, IOManager.PREFS)) {
            try {
                prefs = (DefaultHashMap<String, Object>) IOManager.readObject(IOManager.FILEPATH, IOManager.PREFS);
                if(prefs == null) prefs = new DefaultHashMap<>();
            }catch (ClassCastException e){
                prefs = new DefaultHashMap<>();
            }
        } else {
            prefs = new DefaultHashMap<>();
        }
    }

    public PreferencesManager refresh() {
        if (IOManager.fileExists(IOManager.FILEPATH, IOManager.PREFS)) {
            try {
                prefs = (DefaultHashMap<String, Object>) IOManager.readObject(IOManager.FILEPATH, IOManager.PREFS);
                if(prefs == null) prefs = new DefaultHashMap<>();
            }catch (ClassCastException e){
                prefs = new DefaultHashMap<>();
            }
        } else {
            prefs = new DefaultHashMap<>();
        }
        return this;
    }

    public String getString(String key, String defaultVal) {
        return (String) prefs.get(key, defaultVal);
    }

    public boolean getBoolean(String key, boolean defaultVal) {
        return (boolean) prefs.get(key, defaultVal);
    }

    public int getInteger(String key, int defaultVal) {
        return (int) prefs.get(key, defaultVal);
    }

    public Object get(String key, Object defaultVal) {
        return prefs.get(key, defaultVal);
    }

    public void putAndApply(String key, Object value) {
        prefs.put(key, value);
        IOManager.writeObject(prefs, IOManager.FILEPATH, IOManager.PREFS);
    }

    public PreferencesManager put(String key, Object value) {
        prefs.put(key, value);
        return this;
    }

    public void apply() {
        IOManager.writeObject(prefs, IOManager.FILEPATH, IOManager.PREFS);
    }
}
