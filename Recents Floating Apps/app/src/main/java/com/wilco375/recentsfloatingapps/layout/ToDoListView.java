package com.wilco375.recentsfloatingapps.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wilco375.recentsfloatingapps.R;
import com.wilco375.recentsfloatingapps.general.PreferencesManager;
import com.wilco375.recentsfloatingapps.general.Utils;

import java.util.ArrayList;
import java.util.List;

public class ToDoListView extends LinearLayout {
    public ToDoListView(Context context) {
        super(context);
        init(context, null);
    }

    public ToDoListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, null);
    }

    public ToDoListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, null);
    }

    @TargetApi(21)
    public ToDoListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, null);
    }

    public ToDoListView(Context context, List<String> texts){
        super(context);
        init(context, texts);
    }

    private void init(final Context context, @Nullable List<String> texts){
        if(texts != null && texts.size() > 0){
            for(String text : texts){
                ToDoListViewItem item;
                if(text.split(" - ").length >= 2) item = new ToDoListViewItem(context, text.split(" - ")[1], text.split(" - ")[0].equals("Y"));
                else item = new ToDoListViewItem(context, "", text.split(" - ")[0].equals("Y"));
                item.setOnDeleteListener(new OnDeleteListener() {
                    @Override
                    public void onDelete(ToDoListViewItem item) {
                        if(ToDoListView.this.getChildCount() > 2) {
                            int index = ToDoListView.this.indexOfChild(item);
                            ToDoListView.this.removeViewAt(index);
                            if (index - 1 > 0)
                                ToDoListView.this.getChildAt(index - 1).requestFocus();
                        }
                    }
                });
                addView(item);
            }
        }

        ImageView add = new ImageView(context);
        add.setImageResource(R.drawable.add);
        add.setLayoutParams(new LinearLayout.LayoutParams(Utils.dpToPx(48, context), Utils.dpToPx(48, context)));
        add.setPadding(25, 25, 25, 25);
        add.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ToDoListViewItem item = new ToDoListViewItem(ToDoListView.this.getContext());
                item.setOnDeleteListener(new OnDeleteListener() {
                    @Override
                    public void onDelete(ToDoListViewItem item) {
                        if(ToDoListView.this.getChildCount() > 2) {
                            int index = ToDoListView.this.indexOfChild(item);
                            ToDoListView.this.removeViewAt(index);
                            if (index - 1 > 0)
                                ToDoListView.this.getChildAt(index - 1).requestFocus();
                        }
                    }
                });
                addView(item, getChildCount()-1);
                item.requestFocus();
            }
        });
        addView(add);

        setOrientation(VERTICAL);
    }

    public List<String> getTexts(){
        List<String> texts = new ArrayList<>();
        for(int i = 0; i < getChildCount()-1; i++){
            ToDoListViewItem item = (ToDoListViewItem) getChildAt(i);
            String text = "";

            if(item.isChecked()) text += "Y - ";
            else text += "N - ";

            text += item.getText();

            texts.add(text);
        }
        return texts;
    }

    public String getTextsAsString(){
        String texts = "";
        for(String text : getTexts()){
            texts += (text + "\n");
        }
        return texts;
    }

    private interface OnDeleteListener {
        void onDelete(ToDoListViewItem item);
    }

    private class ToDoListViewItem extends LinearLayout {
        private EditText editText;
        private CheckBox checkBox;
        private OnDeleteListener listener;

        public ToDoListViewItem(Context context, String text, boolean checked){
            super(context);
            init(context, text, checked);
        }

        public ToDoListViewItem(Context context) {
            super(context);
            init(context, null, false);
        }

        public ToDoListViewItem(Context context, AttributeSet attrs) {
            super(context, attrs);
            init(context, null, false);
        }

        public ToDoListViewItem(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init(context, null, false);
        }

        @TargetApi(21)
        public ToDoListViewItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
            init(context, null, false);
        }

        public String getText(){
            return editText.getText().toString();
        }

        public boolean isChecked(){
            return checkBox.isChecked();
        }

        public void setOnDeleteListener(@Nullable OnDeleteListener listener){
            this.listener = listener;
        }

        private void init(Context context, @Nullable String text, boolean checked){
            editText = new EditText(context);
            editText.getBackground().mutate().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
            editText.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus) editText.getBackground().mutate().setColorFilter(Color.parseColor("#"+ (new PreferencesManager()).getString("themeColor","283593")), PorterDuff.Mode.SRC_ATOP);
                    else editText.getBackground().mutate().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
                }
            });
            editText.setTextColor(Color.BLACK);
            editText.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            if(!TextUtils.isEmpty(text)) editText.setText(text);
            editText.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                        if(listener != null && ((EditText) v).getText().length() == 0) listener.onDelete(ToDoListViewItem.this);
                    }else if((keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_NUMPAD_ENTER)
                            && event.getAction() == KeyEvent.ACTION_DOWN){
                        return true;
                    }
                    return false;
                }
            });

            checkBox = new CheckBox(context);
            checkBox.setChecked(checked);
            if(Build.VERSION.SDK_INT >= 21) checkBox.setButtonTintList(
                    new ColorStateList(
                            new int[][] {
                                    new int[] { android.R.attr.state_checked },
                                    new int[] { -android.R.attr.state_checked}
                            },
                            new int[] {
                                    Color.parseColor("#"+ (new PreferencesManager()).getString("themeColor","283593")),
                                    Color.BLACK
                            }
                    )
            );

            addView(checkBox);
            addView(editText);
            setOrientation(HORIZONTAL);
            setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }
}
