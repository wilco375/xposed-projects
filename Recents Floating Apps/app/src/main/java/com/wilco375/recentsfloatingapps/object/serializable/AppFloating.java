package com.wilco375.recentsfloatingapps.object.serializable;

import android.content.Context;
import android.graphics.Bitmap;

import com.wilco375.recentsfloatingapps.floating.FloatingManager;

import java.io.Serializable;

public class AppFloating implements Serializable{

    public AppFloating(){}

    public AppFloating(int type, DefaultHashMap<String, Object> data){
        this.type = type;
        this.data = data;
    }

    public AppFloating(int type){
        this.type = type;
    }

    public int type = 0;
    public DefaultHashMap<String, Object> data = new DefaultHashMap<>();

    public Object getExtraData(String key){
        return this.data.get(key);
    }

    public void putExtraData(String key, Object data){
        this.data.put(key, data);
    }

    public Bitmap getBitmap(Context context, boolean scale){
        return FloatingManager.getBitmap(this, context, scale);
    }

}
