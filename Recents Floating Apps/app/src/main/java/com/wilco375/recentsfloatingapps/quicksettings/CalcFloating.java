package com.wilco375.recentsfloatingapps.quicksettings;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.service.quicksettings.TileService;

import com.wilco375.recentsfloatingapps.floating.FloatingManager;
import com.wilco375.recentsfloatingapps.object.serializable.AppFloating;

@TargetApi(Build.VERSION_CODES.N)
public class CalcFloating extends TileService {
    @Override
    public void onClick() {
        if(!isLocked()){
            FloatingManager.launchFloatingApp(
                    new AppFloating(FloatingManager.CALCULATOR),
                    getApplicationContext()
            );
            Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(it);
        }
    }
}
