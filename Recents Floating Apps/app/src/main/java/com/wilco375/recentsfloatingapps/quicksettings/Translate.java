package com.wilco375.recentsfloatingapps.quicksettings;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.service.quicksettings.TileService;

@TargetApi(Build.VERSION_CODES.N)
public class Translate extends TileService {
    @Override
    public void onClick() {
        if(!isLocked()){
            String packageName = "com.google.android.apps.translate";
            Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);

            if(intent == null) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+packageName));
            }
            startActivity(intent);
            Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(it);
        }
    }
}
