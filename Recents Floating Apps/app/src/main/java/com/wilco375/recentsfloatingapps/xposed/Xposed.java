package com.wilco375.recentsfloatingapps.xposed;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.wilco375.recentsfloatingapps.BuildConfig;
import com.wilco375.recentsfloatingapps.floating.FloatingManager;
import com.wilco375.recentsfloatingapps.general.IOManager;
import com.wilco375.recentsfloatingapps.general.PreferencesManager;
import com.wilco375.recentsfloatingapps.general.Utils;
import com.wilco375.recentsfloatingapps.object.serializable.AppFloating;

import java.util.ArrayList;
import java.util.List;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static de.robv.android.xposed.XposedHelpers.callMethod;
import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import static de.robv.android.xposed.XposedHelpers.findClass;

public class Xposed implements IXposedHookInitPackageResources, IXposedHookLoadPackage {
    Context recentsContext;

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals("com.android.systemui")) return;

        findAndHookMethod(findClass("com.android.systemui.recents.RecentsActivity", lpparam.classLoader), "onCreate", Bundle.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                recentsContext = (Activity) param.thisObject;
                super.beforeHookedMethod(param);
            }
        });
    }

    @Override
    public void handleInitPackageResources(final XC_InitPackageResources.InitPackageResourcesParam resparam) throws Throwable {
        if (!resparam.packageName.equals("com.android.systemui")) return;

        resparam.res.hookLayout("com.android.systemui", "layout", "recents", new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam liparam) throws Throwable {
                if(recentsContext == null){
                    XposedBridge.log("[Recents Floating Apps] recentsContext is null, returning");
                    return;
                }
                try {
                    FrameLayout topView = (FrameLayout) liparam.view;

                    final Context appContext = recentsContext.createPackageContext(BuildConfig.APPLICATION_ID, Context.CONTEXT_IGNORE_SECURITY);

                    if(IOManager.fileExists(IOManager.FILEPATH, IOManager.APPS_FLOATING)){
                        try {
                            final PreferencesManager preferencesManager = new PreferencesManager();

                            List<AppFloating> floatingApps = (ArrayList<AppFloating>) IOManager.readObject(IOManager.FILEPATH, IOManager.APPS_FLOATING);

                            final FloatingActionsMenu fam = new FloatingActionsMenu(appContext);
                            FrameLayout.LayoutParams famLayoutParams =
                                    new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                            if(preferencesManager.getBoolean("showLeft", false))
                                famLayoutParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
                            else
                                famLayoutParams.gravity = Gravity.BOTTOM | Gravity.RIGHT;
                            //l t r b
                            famLayoutParams.setMargins(
                                    Utils.dpToPx(preferencesManager.getInteger("distanceSide", 16), recentsContext),
                                    0,
                                    Utils.dpToPx(preferencesManager.getInteger("distanceSide", 16), recentsContext),
                                    Utils.dpToPx(preferencesManager.getInteger("distanceBottom", 16), recentsContext) + Utils.getNavBarHeight(recentsContext));
                            fam.setLayoutParams(famLayoutParams);
                            fam.setColorNormal(Color.parseColor("#"+preferencesManager.getString("menuColor","E91E63")));
                            fam.setColorPressed(Utils.darker(Color.parseColor("#"+preferencesManager.getString("themeColor","E91E63")), 0.8f));

                            int themeColor = Color.parseColor("#"+preferencesManager.getString("themeColor","283593"));
                            int themeColorDarker = Utils.darker(Color.parseColor("#"+preferencesManager.getString("themeColor","283593")), 0.8f);

                            for(final AppFloating floatingApp : floatingApps){
                                final FloatingActionButton fab = new FloatingActionButton(appContext);
                                fab.setColorNormal(themeColor);
                                fab.setColorPressed(themeColorDarker);
                                fab.setPadding(0, 0, 0, Utils.dpToPx(6, recentsContext));
                                fab.setImageBitmap(floatingApp.getBitmap(appContext, true));
                                fab.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        FloatingManager.launchFloatingApp(floatingApp, recentsContext);
                                        preferencesManager.refresh();
                                        if(preferencesManager.getBoolean("closeMenu", true))
                                            fam.collapse();
                                        if(preferencesManager.getBoolean("closeRecents", true))
                                            callMethod(recentsContext, "dismissRecentsToFocusedTaskOrHome", false);
                                    }
                                });
                                fam.addButton(fab);
                            }

                            if(fam.getChildCount() > 0) topView.addView(fam);

                        }catch (ClassCastException e){
                            e.printStackTrace();
                        }
                    }
                    XposedBridge.log("[Recents Floating Apps] View added");
                }catch (ClassCastException e){
                    XposedBridge.log("[Recents Floating Apps] Casting to FrameLayout failed");
                    e.printStackTrace();
                }
            }
        });
    }
}
