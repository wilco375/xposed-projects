package com.wilco375.recentsfloatingapps.licensing;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;

import com.wilco375.recentsfloatingapps.R;

public class License {
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiUoWBWnWot8/1mekBQL2Hv5dzK5OEs8C+9hOjEp/Bf1a6C8d5SEK3svjyseKSP15vvUDbHCyDRdOdt7CHRv4S2CuIBPaKBtQl06pi4qEdyrP0GwBQ5VtAukijZ6gTSbrBosrLlCxFc/EtBY4f+ZSCx6p/0E3tRdgkcaR50M4ulqR7ZNsN/ml1KWwMZwfmPs6gJ1IqO8b406nj+TRiEO5eiBQKq7fcicYYfyLzoUit0Aj7wnl7MKjbeYYwPzAOZVXBKclE5CPCkP7tWa2zqKlCHJBA165g19g5fpU7bc/X9dsG3tPOnAHYepTIvlVhqmVNiOdA0NE0C+mCNfKlwR5GwIDAQAB";
    private static final byte[] SALT = new byte[] {30, 45, 48, 51, 23, 12, 16, 11, 54, 25};

    private boolean licensed;
    private boolean checkingLicense;
    private boolean didCheck;
    private Activity activity;

    private @Nullable ProgressDialog licenseDialog;

    public void checkLicense(Activity activity) {
        if(!licensed && !checkingLicense) {
            didCheck = false;
            checkingLicense = true;
            this.activity = activity;

            showLicenseCheckingDialog();

            String deviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
            Log.i("Device Id", deviceId);

            LicenseCheckerCallback licenseCheckerCallback = new MyLicenseCheckerCallback();
            LicenseChecker checker = new LicenseChecker(activity, new ServerManagedPolicy(activity, new AESObfuscator(SALT, activity.getPackageName(), deviceId)), BASE64_PUBLIC_KEY);

            checker.checkAccess(licenseCheckerCallback);
        }
    }

    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {

        @Override
        public void allow(int reason) {
            Log.i("License", "Accepted!");

            licensed = true;
            checkingLicense = false;
            didCheck = true;

            if(licenseDialog != null){
                licenseDialog.cancel();
            }
        }

        @Override
        public void dontAllow(int reason) {
            Log.i("License", "Denied!");
            Log.i("License", "Reason for denial: " + reason);

            licensed = false;
            checkingLicense = false;
            didCheck = true;

            showLicenseFailDialog(reason);
        }

        @Override
        public void applicationError(int reason) {
            Log.i("License", "Application error!");

            licensed = true;
            checkingLicense = false;
            didCheck = false;

            showLicenseFailDialog(-1);
        }
    }

    private void showLicenseFailDialog(final int reason){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setCancelable(false);
                builder.setTitle(R.string.license_fail_title);
                builder.setMessage(activity.getResources().getString(R.string.license_fail_desc, reason));
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                });
                builder.setNeutralButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkLicense(activity);
                        dialog.cancel();
                    }
                });
                Dialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);

                if(licenseDialog != null){
                    licenseDialog.cancel();
                }

                dialog.show();
            }
        });
    }

    private void showLicenseCheckingDialog(){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                licenseDialog = new ProgressDialog(activity);
                licenseDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                licenseDialog.setMessage(activity.getResources().getString(R.string.checking_license));
                licenseDialog.setIndeterminate(true);
                licenseDialog.setCanceledOnTouchOutside(false);
                licenseDialog.setCancelable(false);
                licenseDialog.setProgressNumberFormat(null);
                licenseDialog.setProgressPercentFormat(null);
                licenseDialog.show();
            }
        });
    }
}
